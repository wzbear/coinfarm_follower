from datetime import datetime
from utils import database
from sentiments import followtoptrader
from utils.log import setup_custom_logger
import logging
setup_custom_logger('order follow main', level=logging.INFO)
logger = logging.getLogger('order follow main')
c = followtoptrader.CoinfarmFollower()
c.check_time_min = 1
# print(c.check_time_min)
# c.fetch_long()
# c.check_user_profile('zeroparty', True)
# print("long trades")
c.filter_user()
# print(c.long_users)
# print("short trades")
c.filter_user(False)
# print(c.short_users)
for user in c.long_users:
    time = datetime.strptime(user['order_time'], '%Y-%m-%d %H:%M')
    userid = database.add_position_follow('XBTUSD', user['id'], time, float(user['enter_price']), int(user['qty']))
    logger.info('added long user %s' % user['id'])

for user in c.short_users:
    time = datetime.strptime(user['order_time'], '%Y-%m-%d %H:%M')
    userid = database.add_position_follow('XBTUSD', user['id'], time, float(user['enter_price']), int(user['qty']))
    logger.info('added short user %s' % user['id'])
from datetime import datetime
from datetime import timedelta
import math
from time import sleep
from sentiments import binance, coinfarm
last_run_min = -1
interval = 5

while True:
    now = datetime.now()
    if now.minute % interval == 0 and math.floor(now.minute/interval) != last_run_min:
        last_run_min = math.floor(now.minute/interval)
        print("run sentiment at", now)
        c = coinfarm.Coinfarm()
        c.trade_counter()
        c.buy_sell(5)
        c.buy_sell(60)


        s = binance.Binance()
        s.global_long_short("BTCUSDT", 5)
        s.global_long_short("BTCUSDT", 60)
        sleep(1)
        s.top_long_short_position("BTCUSDT", 5)
        s.top_long_short_position("BTCUSDT", 60)
        sleep(1)
        s.top_long_short("BTCUSDT", 5)
        s.top_long_short("BTCUSDT", 60)
        sleep(1)
    sleep(1)
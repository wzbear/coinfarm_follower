import requests
from datetime import datetime
import sys
from os.path import dirname, join, abspath
sys.path.insert(0, abspath(join(dirname(__file__), '..')))
from utils import database
from time import sleep

#Binance global long/short ratio

# global-long-short
# top-long-short-position
# top-long-short

class Binance(object):
    def global_long_short(self, symbol, min):
        type = "global-long-short"
        url = "https://www.binance.com/gateway-api/v1/public/future/data/global-long-short-account-ratio"
        data = '{"name":"%s","periodMinutes": %d}' % (symbol, int(min))
        headers = {'content-type': 'application/json'}
        response = requests.post(url, data=data, headers=headers)
        r = response.json()
        time = r["data"]["xAxis"]
        ratio = r["data"]["series"][0]["data"]
        long = r["data"]["series"][1]["data"]
        short = r["data"]["series"][2]["data"]
        length = len(time)
        counter = 0
        for x in range(length):
            t = datetime.fromtimestamp(time[x]/1000)
            if database.add_binance_longshort(symbol, str(min) + "m", ratio[x], long[x], short[x], type, t):
                counter += 1
        # print(type, "records for", min, "minutes added", counter, " times")

    def top_long_short(self, symbol, min):
        type = "top-long-short"
        url = "https://www.binance.com/gateway-api/v1/public/future/data/long-short-account-ratio"
        data = '{"name":"%s","periodMinutes": %d}' % (symbol, int(min))
        headers = {'content-type': 'application/json'}
        response = requests.post(url, data=data, headers=headers)
        r = response.json()
        time = r["data"]["xAxis"]
        ratio = r["data"]["series"][0]["data"]
        long = r["data"]["series"][1]["data"]
        short = r["data"]["series"][2]["data"]
        length = len(time)
        counter = 0
        for x in range(length):
            t = datetime.fromtimestamp(time[x]/1000)
            if database.add_binance_longshort(symbol, str(min) + "m", ratio[x], long[x], short[x], type, t):
                counter += 1
        # print(type, "records for", min, "minutes added", counter, " times")

    def top_long_short_position(self, symbol, min):
        type = "top-long-short-position"
        url = "https://www.binance.com/gateway-api/v1/public/future/data/long-short-position-ratio"
        data = '{"name":"%s","periodMinutes": %d}' % (symbol, int(min))
        headers = {'content-type': 'application/json'}
        response = requests.post(url, data=data, headers=headers)
        r = response.json()
        time = r["data"]["xAxis"]
        ratio = r["data"]["series"][0]["data"]
        long = r["data"]["series"][1]["data"]
        short = r["data"]["series"][2]["data"]
        length = len(time)
        counter = 0
        for x in range(length):
            t = datetime.fromtimestamp(time[x] / 1000)
            if database.add_binance_longshort(symbol, str(min) + "m", ratio[x], long[x], short[x], type, t):
                counter += 1
        # print(type, "records for", min, "minutes added", counter, " times")

if __name__ == "__main__":
    s = Binance()
    s.global_long_short("BTCUSDT", 5)
    s.global_long_short("BTCUSDT", 60)
    sleep(1)
    s.top_long_short_position("BTCUSDT", 5)
    s.top_long_short_position("BTCUSDT", 60)
    sleep(1)
    s.top_long_short("BTCUSDT", 5)
    s.top_long_short("BTCUSDT", 60)



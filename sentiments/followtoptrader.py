import requests
from datetime import datetime
from datetime import timedelta
import sys
import logging
from os.path import dirname, join, abspath
sys.path.insert(0, abspath(join(dirname(__file__), '..')))
from utils import database
from bs4 import BeautifulSoup
from utils.log import setup_custom_logger

class CoinfarmFollower(object):
    def __init__(self):
        setup_custom_logger('order follow', level=logging.INFO)
        self.logger = logging.getLogger('order follow')
        self.long_users = []
        self.short_users = []
        self.check_time_min = 30
        self.min_hit_rate = 90
        self.min_win_ratio = 0.5
        self.min_total_trade = 5
        self.min_average_trade_qty = 10000
        self.debug_on = False


    def filter_user(self, flag=True):
        new_users = []
        if flag:
            self.fetch_users(flag)
            for user in self.long_users:
                if self.check_user_profile(user['id'], flag):
                    new_users.append(user)
            self.long_users = new_users

        else:
            self.fetch_users(flag)
            for user in self.short_users:
                if self.check_user_profile(user['id'], flag):
                    new_users.append(user)
            self.short_users = new_users


    def check_user_profile(self, user_id, long=True):
        url = "https://www.coinfarm.online/position/ajax_mex_long_sc.asp?id=%s&gmt=+0:00" % user_id
        if long == False:
            url = "https://www.coinfarm.online/position/ajax_mex_short_sc.asp?id=%s&gmt=+0:00" % user_id
        headers = {'content-type': 'application/json'}
        response = requests.post(url, headers=headers)
        html = response.content
        soup = BeautifulSoup(html, 'html.parser')
        table = soup.find('table')
        total_trade = 0
        profit_30_min = 0
        profit_60_min = 0
        total_qty = 0
        total_30min = 0
        total_60min = 0
        net_30min_win_ratio = 0
        net_60min_win_ratio = 0
        hit_30min = 0
        hit_60min = 0
        average_trade_qty = 0
        try:
            for row in table.find_all('tr')[1:]:
                td = row.find_all('td')
                user = {
                    "id": td[1].text,
                    "order_time": td[3].text.replace(',', ''),
                    "enter_price": td[4].text.replace(',', ''),
                    "qty": td[5].text.replace(',', ''),
                    "30min_price": td[6].text.replace(',', ''),
                    "30min_profit": td[7].text.replace(',', ''),
                    "60min_price": td[8].text.replace(',', ''),
                    "60min_profit": td[9].text.replace(',', '')
                }
                if self.debug_on:
                    print(user)
                if int(user['30min_profit']) != 0 and int(user['60min_profit']) != 0:
                    if int(user['30min_profit']) > 0:
                        profit_30_min += 1
                    if int(user['60min_profit']) > 0:
                        profit_60_min += 1

                    total_qty += abs(int(user['qty']))
                    total_30min += int(user['30min_profit'])
                    total_60min += int(user['60min_profit'])
                    total_trade += 1
            if total_qty > 0:
                net_30min_win_ratio = round(total_30min/total_qty*100, 2)
                net_60min_win_ratio = round(total_60min/total_qty*100, 2)
            if total_trade > 0:
                average_trade_qty = round(total_qty/total_trade, 2)
                hit_30min = round(profit_30_min/total_trade*100, 2)
                hit_60min = round(profit_60_min/total_trade*100, 2)
            self.logger.info('At %s, detect user with id: %s, total trade: %s, average qty: %s, 30min hit rate: %s%%, 60 min hit rate: %s%%, 30min profit: %s%%, 60min profit: %s%% \n'
                             % (datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"), user_id, total_trade, average_trade_qty, hit_30min, hit_60min, net_30min_win_ratio, net_60min_win_ratio))


            if total_trade > self.min_total_trade and average_trade_qty > self.min_average_trade_qty and (hit_30min >= self.min_hit_rate or hit_60min >= self.min_hit_rate) \
                    and (net_30min_win_ratio > self.min_win_ratio or net_60min_win_ratio > self.min_win_ratio):
                self.logger.info('analyse decision: follow this user %s\n', user_id)
                return True
            else:
                self.logger.info('analyse decision: ignore this user %s\n', user_id)
                return False
        except Exception as e:
            self.logger.error("error from followtoptrader", exc_info=True)
            return False


    def fetch_users(self, long=True):
        if long:
            self.long_users = []
        else:
            self.short_users = []
        now = datetime.utcnow()
        url = "https://www.coinfarm.online/position/ajax_bbs_long_pr_t.asp?am=0&data_type=0&gmt=+0:00"
        if long == False:
            url = "https://www.coinfarm.online/position/ajax_bbs_short_pr_t.asp?am=0&data_type=0&gmt=+0:00"
        headers = {'content-type': 'application/json'}
        response = requests.post(url, headers=headers)
        html = response.content
        soup = BeautifulSoup(html, 'html.parser')
        # print(soup.prettify())
        table = soup.find('table')

        for row in table.find_all('tr')[1:]:
            td = row.find_all('td')
            if td[2].find('i') == None:
                continue
            strdate = td[3].text.replace(str(now.year), now.strftime("%Y-%m-%d"))
            time = datetime.strptime(td[3].text.replace(str(now.year), now.strftime("%Y-%m-%d")), '%Y-%m-%d %H:%M')
            user = {
                "id": td[1].text,
                "order_time": strdate,
                "islive":td[2].html,
                "enter_price": td[4].text.replace(',', ''),
                "qty": td[5].text.replace(',', ''),
                "30min_price": td[6].text.replace(',', ''),
                "30min_profit": td[7].text.replace(',', ''),
                "60min_price": td[8].text.replace(',', ''),
                "60min_profit": td[9].text.replace(',', ''),
                "current_profit": td[10].text.replace(',', '')
            }
            if self.debug_on:
                print(user)
            if time < now-timedelta(minutes=self.check_time_min):
                break
            if long:
                self.long_users.append(user)
            else:
                self.short_users.append(user)





if __name__ == "__main__":
    c = CoinfarmFollower()
    c.debug_on = True
    # c.check_time_min = 70
    # c.fetch_users(long=False)
    c.check_user_profile('stock.P', False)
    # print("long trades")
    # c.filter_user()
    # print(c.long_users)
    # print("short trades")
    # c.filter_user(False)
    # print(c.short_users)
    # for user in c.long_users:
    #     time = datetime.strptime(user['order_time'], '%Y-%m-%d %H:%M')
    #     userid = database.add_position_follow('XBTUSD', user['id'], time, float(user['enter_price']), int(user['qty']))
    #     print(userid, 'added')
    #
    # for user in c.short_users:
    #     time = datetime.strptime(user['order_time'], '%Y-%m-%d %H:%M')
    #     userid = database.add_position_follow('XBTUSD', user['id'], time, float(user['enter_price']), int(user['qty']))
    #     print(userid, 'added')
import requests
from datetime import datetime
import sys
from os.path import dirname, join, abspath
sys.path.insert(0, abspath(join(dirname(__file__), '..')))
from utils import database
from time import sleep

class Coinfarm(object):
    def trade_counter(self, symbol="XBTUSD"):
        url = "https://www.coinfarm.online/public/api/mex_trade_usd.asp"
        headers = {'content-type': 'application/json'}
        response = requests.post(url, headers=headers)
        r = response.json()
        database.add_coinbase_trade_counter(symbol, int(r['b5']), int(r['s5']), int(r['b10']), int(r['s10']),
                                            int(r['b15']), int(r['s15']), int(r['b30']), int(r['s30']),
                                            int(r['b60']), int(r['s60']), int(r['b120']), int(r['s120']))
    def buy_sell(self, timeframe, symbol="XBTUSD"):
        url = "https://www.coinfarm.online/public/gauge/json_time_tot.asp?target=bitmex&times="+str(timeframe)
        headers = {'content-type': 'application/json'}
        response = requests.post(url, headers=headers)
        r = response.json()
        price = self.current_price()
        database.add_coinbase_buy_sell(symbol, float(r['buy']), float(r['sell']), int(timeframe), price)

    def current_price(self):
        url = "https://www.coinfarm.online/public/api/ajax_mex_btc_en.asp"
        headers = {'content-type': 'application/json'}
        response = requests.post(url, headers=headers)
        r = response.json()
        price = float(r['btc'])
        print(price)
        return price


if __name__ == "__main__":
    s = Coinfarm()
    # s.trade_counter()
    # s.buy_sell(5)
    # s.buy_sell(60)
    # s.current_price()
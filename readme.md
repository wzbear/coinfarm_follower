#setup environment
python3 -m venv env
#pip freeze > requirements.txt
pip install -r requirements.txt



#setup database
install mysql
setup connection string in db/db_helper.py
create database coinfarm_follower

#deploy to linux
setup systemd service

#deploy to windows
download nssm nssm.cc
nssm install <servicename>
nssm edit <servicename>
nssm remove <servicename>

1. setup run_follower.py
Path: C:\Users\wz\workspace\coinfarm_follower\env\Scripts\python.exe
dir: C:\Users\wz\workspace\coinfarm_follower\
param: C:\Users\wz\workspace\coinfarm_follower\run_follower.py
IO stdout: C:\Users\wz\workspace\coinfarm_follower\log\ws_log.log
IO stderr: C:\Users\wz\workspace\coinfarm_follower\log\ws_log.log
File rotate: 1000000
Exit restart: 5000

2. setup run_sentiment.py
Path: C:\Users\wz\workspace\coinfarm_follower\env\Scripts\python.exe
dir: C:\Users\wz\workspace\coinfarm_follower\
param: C:\Users\wz\workspace\coinfarm_follower\run_sentiment.py
IO stdout: C:\Users\wz\workspace\coinfarm_follower\log\ws_log.log
IO stderr: C:\Users\wz\workspace\coinfarm_follower\log\ws_log.log
File rotate: 1000000
Exit restart: 5000


#install ta-lib
download TA_Lib‑0.4.17‑cp38‑cp38‑win32.whl from
https://www.lfd.uci.edu/~gohlke/pythonlibs/#ta-lib
run
pip install TA_Lib‑0.4.17‑cp38‑cp38‑win32.whl





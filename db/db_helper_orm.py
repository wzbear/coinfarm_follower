from datetime import datetime
from sqlalchemy import Column, DateTime, Integer, String, Float, Boolean, create_engine, BigInteger, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = create_engine('mysql+pymysql://root:root@localhost/coinfarm_follower', echo=False)
Base = declarative_base()
Session = sessionmaker(bind=engine)

class WSData(Base):
    __tablename__ = 'ws_data'
    id = Column(Integer, primary_key=True)
    symbol = Column(String(32))
    type = Column(String(32))
    value = Column(String(100))
    timestamp = Column(DateTime, default=datetime.now)

class TradingData(Base):
    __tablename__ = 'trading_data'
    id = Column(Integer, primary_key=True)
    symbol = Column(String(32))
    order_id = Column(String(50))
    entry_price = Column(Float)
    qty = Column(Integer)
    direction = Column(String(32)) #long short
    position = Column(String(32)) #pending open close
    take_profit = Column(Float)
    stop_lose = Column(Float)
    trailing_stop = Column(Float) #
    stop_lose_time = Column(Integer)
    timestamp = Column(DateTime, default=datetime.now)

class CoinfarmPositionFollow(Base):
    __tablename__ = 'coinfarm_position_follow'
    id = Column(BigInteger, primary_key=True)
    symbol = Column(String(32))
    userID = Column(String(50))
    ordertime = Column(DateTime)
    enterprice = Column(Float)
    qty = Column(Integer)
    followed = Column(Boolean, default=False)
    myenterprice = Column(Float, default=0)
    myentertime = Column(DateTime, default=None)
    myquitprice = Column(Float, default=0)
    quit = Column(Boolean, default=False)
    timestamp = Column(DateTime, default=datetime.now)

class CoinfarmBuySellIndicator(Base):
    __tablename__ = 'coinfarm_buy_sell'
    id = Column(BigInteger, primary_key=True)
    symbol = Column(String(32))
    buy = Column(Float)
    sell = Column(Float)
    timeframe = Column(Integer)
    price = Column(Float)
    timestamp = Column(DateTime, default=datetime.now)

class CoinfarmTradeCounter(Base):
    __tablename__ = 'coinfarm_trade_counter'
    id = Column(BigInteger, primary_key=True)
    symbol = Column(String(32))
    b5 = Column(Integer)
    s5 = Column(Integer)
    b10 = Column(Integer)
    s10 = Column(Integer)
    b15 = Column(Integer)
    s15 = Column(Integer)
    b30 = Column(BigInteger)
    s30 = Column(BigInteger)
    b60 = Column(BigInteger)
    s60 = Column(BigInteger)
    b120 = Column(BigInteger)
    s120 = Column(BigInteger)
    timestamp = Column(DateTime, default=datetime.now)

class BinanceLongshort(Base):
    __tablename__ = 'binance_long_short'
    id = Column(Integer, primary_key=True)
    symbol = Column(String(32))
    timeframe = Column(String(10))
    ratio = Column(Float)
    long = Column(String(10))
    short = Column(String(10))
    type = Column(String(40))
    time = Column(DateTime)
    sys_timestamp = Column(DateTime, default=datetime.now)


class Liquidation(Base):
    __tablename__ = 'liquidation'
    id = Column(Integer, primary_key=True)
    symbol = Column(String(32))
    order_id = Column(String(100))
    side = Column(String(10))
    price = Column(Float)
    qty = Column(Integer)
    usd_value = Column(Float)
    type = Column(String(10))
    buy = Column(Float)
    sell = Column(Float)
    timestamp = Column(DateTime, default=datetime.now)

class HistoryBucket(Base):
    __tablename__ = 'history_bucket'
    id = Column(Integer, primary_key=True)
    symbol = Column(String(32))
    interval = Column(String(10))
    time = Column(DateTime)
    open = Column(Float)
    close = Column(Float)
    high = Column(Float)
    low = Column(Float)
    volume = Column(Integer)

class LiveOrders(Base):
    __tablename__ = 'live_orders'
    id = Column(Integer, primary_key=True)
    symbol = Column(String(32))
    open_id = Column(String(300))
    open_time = Column(DateTime)
    open_quantity = Column(Integer)
    close_id = Column(String(300))
    close_quantity = Column(Integer)
    status = Column(String(100))
    desired_quantity = Column(Integer)
    desired_open_price = Column(Float)
    desired_close_price = Column(Float)
    is_short = Column(Boolean)


class HistoryOrders(Base):
    __tablename__ = 'history_orders'
    id = Column(Integer, primary_key=True)
    symbol = Column(String(32))
    open_id = Column(String(300))
    open_time = Column(DateTime)
    open_quantity = Column(Integer)
    open_price = Column(Float)
    close_id = Column(String(300))
    close_quantity = Column(Integer)
    close_price = Column(Float)
    profit = Column(Float)
    is_short = Column(Boolean)


class Logs(Base):
    __tablename__ = 'logs'
    id = Column(Integer, primary_key=True)
    level = Column(String(100))
    message = Column(String(2000))
    timestamp = Column(DateTime, default=datetime.now)

class OrderBook(Base):
    __tablename__ = 'order_book'
    id = Column(BigInteger, primary_key=True)
    symbol = Column(String(32))
    up_orderbook = Column(Text)
    down_orderbook = Column(Text)
    timestamp = Column(DateTime, default=datetime.now)

class OrderCounter(Base):
    __tablename__ = 'order_counter'
    id = Column(BigInteger, primary_key=True)
    symbol = Column(String(32))
    buy_counter = Column(Integer)
    sell_counter = Column(Integer)
    buy_size = Column(Integer)
    sell_size = Column(Integer)
    quote_price = Column(Float)
    trade_price = Column(Float)
    timestamp = Column(DateTime, default=datetime.now)

class OrderFlow(Base):
    __tablename__ = 'order_flow'
    id = Column(BigInteger, primary_key=True)
    symbol = Column(String(32))
    side = Column(String(10))
    price = Column(Float)
    size = Column(Integer)
    trade_time = Column(String(30))
    trade_id = Column(String(50))
    timestamp = Column(DateTime, default=datetime.now)

class OrderFlowAnalyser(Base):
    __tablename__ = 'order_flow_analyser'
    id = Column(BigInteger, primary_key=True)
    symbol = Column(String(32))
    open_price = Column(Float)
    close_price = Column(Float)
    high_price = Column(Float)
    low_price = Column(Float)
    buy_counter = Column(Integer)
    sell_counter = Column(Integer)
    buy_size = Column(Integer)
    sell_size = Column(Integer)
    large_buy_counter = Column(Integer)
    large_sell_counter = Column(Integer)
    large_buy_size = Column(Integer)
    large_sell_size = Column(Integer)
    small_buy_sell_ratio = Column(Float)
    big_buy_sell_ratio = Column(Float)
    timestamp = Column(DateTime)
    system_timestamp = Column(DateTime, default=datetime.now)

class Pnl(Base):
    __tablename__ = 'pnl_history'
    id = Column(BigInteger, primary_key=True)
    ask = Column(Float)
    bid = Column(Float)
    timestamp = Column(DateTime, default=datetime.now)

class Settings(Base):
    __tablename__ = 'settings'
    id = Column(Integer, primary_key=True)
    name = Column(String(200))
    value = Column(String(2000))
    timestamp = Column(DateTime, default=datetime.now)


Base.metadata.create_all(engine)


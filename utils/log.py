import logging
import logging.handlers


def setup_custom_logger(name, level=logging.INFO, interval_hour=2):
    logger = logging.getLogger(name)
    logger.setLevel(level)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(module)s - %(funcName)s - %(lineno)04d - %(message)s')
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    # filename = "log/ziwalog-"+name+".log"
    # file_handler = logging.handlers.TimedRotatingFileHandler(filename, when='H', interval=interval_hour)
    # file_handler.setFormatter(formatter)
    logger.addHandler(console_handler)
    return logger



import datetime
import settings
from sqlalchemy import and_
from db import db_helper_orm
from time import sleep
from utils.log import setup_custom_logger
from utils import math
from utils import helper
import logging
import sys

info_tag = "info"
error_tag = "error"
warning_tag = "warning"

order_active = "order_active"  # order open
position_open = "position_open"  # on position, waiting for close
setup_custom_logger('database', level=settings.DB_LOG_LEVEL)
logger = logging.getLogger('database')

class Grid(object):
    def __init__(self, symbol, desired_open_price, desired_close_price, desired_quantity, status, open_id, close_id, open_quantity, close_quantity):
        self.symbol = symbol
        self.desired_open_price = desired_open_price
        self.desired_close_price = desired_close_price
        self.desired_quantity = desired_quantity
        self.status = status
        self.open_id = open_id
        self.close_id = close_id
        self.open_quantity = open_quantity
        self.close_quantity = close_quantity
        
    def __str__(self):
        return 'Grid setup at open price %f, Quantity %d, close price %f' % (self.desired_open_price, self.desired_quantity, self.desired_close_price)


class Trade(object):
    def __init__(self, open_price, open_id, open_time, close_price, close_id, close_time, profit):
        self.open_price = open_price
        self.open_id = open_id
        self.open_time = open_time
        self.close_price = close_price
        self.close_id = close_id
        self.close_time = close_time
        self.profit = 0

class TradeData(object):
    def __init__(self, symbol, order_id, entry_price, qty, direction, position, take_profit, stop_lose, trailing_stop, stop_lose_time):
        self.symbol = symbol
        self.order_id = order_id
        self.entry_price = entry_price
        self.qty = qty
        self.direction = direction
        self.position = position
        self.take_profit = take_profit
        self.stop_lose = stop_lose
        self.trailing_stop = trailing_stop
        self.stop_lose_time = stop_lose_time




# database log actions
def info(message):
    log_db(message, info_tag)


def warning(message):
    log_db(message, warning_tag)


def error(message):
    log_db(message, error_tag)


def log_db(message, level):
    session = db_helper_orm.Session()
    try:
        logs = db_helper_orm.Logs(level=level, message=message)
        session.add(logs)
        session.commit()
    except:
        session.rollback()
        logger.error(sys.exc_info())
    finally:
        session.close()


# save statistics
def save_statistics(ask, bid, short_count, short_max_step, long_count, long_max_step):
    session = db_helper_orm.Session()
    try:
        statistic = db_helper_orm.Statistics(
            ask=ask,
            bid=bid,
            short_count=short_count,
            short_max_step=short_max_step,
            long_count=long_count,
            long_max_step=long_max_step
        )
        session.add(statistic)
        session.commit()
    except:
        session.rollback()
        logger.error(sys.exc_info())
    finally:
        session.close()

# extra
def clear_db():
    session = db_helper_orm.Session()
    try:
        # delete log
        session.execute("Truncate table logs")
        session.execute("Truncate table settings")
        session.execute("Truncate table live_orders")
        session.execute("Truncate table history_orders")
        session.execute("Truncate table ws_data")
        session.commit()
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()

def clear_db_without_ws_data():
    session = db_helper_orm.Session()
    try:
        # delete log
        session.execute("Truncate table logs")
        session.execute("Truncate table settings")
        session.execute("Truncate table live_orders")
        session.execute("Truncate table history_orders")
        session.commit()
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()

def update_trade_data(symbol, order_id, entry_price, qty, direction, position, take_profit, stop_lose, trailing_stop, stop_lose_time):
    session = db_helper_orm.Session()
    try:
        result = session.query(db_helper_orm.TradingData).filter(
            and_(
                db_helper_orm.TradingData.symbol == symbol,
                db_helper_orm.TradingData.order_id == order_id)
        )
        if result.count() == 0:
            trading = db_helper_orm.TradingData(symbol=symbol, order_id=order_id,
                                                entry_price=entry_price, qty=qty,
                                                direction=direction, position=position,
                                                take_profit=take_profit, stop_lose=stop_lose,
                                                trailing_stop=trailing_stop, stop_lose_time=stop_lose_time)
            session.add(trading)
            session.commit()
            return trading.id
        else:
            data = result.one()
            data.entry_price = entry_price
            data.qty = qty
            data.direction = direction
            data.position = position
            data.take_profit = take_profit
            data.stop_lose = stop_lose
            data.trailing_stop = trailing_stop
            data.stop_lose_time = stop_lose_time
            data.timestamp = datetime.datetime.now()
            session.commit()
            return data.id
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()


def load_all_live_trade(time):
    session = db_helper_orm.Session()
    try:
        result = session.query(db_helper_orm.TradingData).filter(
            and_(
                db_helper_orm.TradingData.position != "closed"
            )
        )
        rows = []
        for r in result:
            row = TradeData(r.symbol, r.order_id, r.entry_price, r.qty, r.direction, r.position,
                            r.take_profit, r.stop_lose, r.trailing_stop, r.stop_lose_time)
            rows.append(row)
        return rows
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()
    return None

def add_position_follow(symbol, userID, ordertime, enterprice, qty):
    session = db_helper_orm.Session()
    try:
        result = session.query(db_helper_orm.CoinfarmPositionFollow).filter(
            and_(db_helper_orm.CoinfarmPositionFollow.symbol == symbol,
                 db_helper_orm.CoinfarmPositionFollow.userID == userID,
                 db_helper_orm.CoinfarmPositionFollow.ordertime == ordertime,
                 db_helper_orm.CoinfarmPositionFollow.enterprice == enterprice,
                 db_helper_orm.CoinfarmPositionFollow.qty == qty))
        if result.count() == 0:
            position = db_helper_orm.CoinfarmPositionFollow(symbol=symbol, userID=userID,
                                                             ordertime=ordertime, enterprice=enterprice,
                                                             qty=qty)
            session.add(position)
            session.commit()
            return position.id
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()
    return 0

# liquidation
def save_liquidation(symbol, order_id, side, price, qty, usd_value, type, buy, sell):
    session = db_helper_orm.Session()
    try:
        order_liquidation = db_helper_orm.Liquidation(symbol=symbol, order_id=order_id,
                                                    side=side, price=price, qty=qty, usd_value=usd_value,
                                                    type=type, buy=buy, sell=sell)
        session.add(order_liquidation)
        session.commit()
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()

def load_liquidation(time):
    session = db_helper_orm.Session()
    try:
        result = session.query(db_helper_orm.Liquidation).filter(
            and_(
                db_helper_orm.Liquidation.timestamp >= time-datetime.timedelta(seconds=60),
                db_helper_orm.Liquidation.timestamp < time
            )
        )
        rows = []
        for r in result:
            row = {
                "symbol": r.symbol,
                "side": r.side,
                "price": r.price,
                "usd_value": r.usd_value,
                "order_id": r.order_id,
                "time": r.timestamp
            }
            rows.append(row)
        return rows
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()
    return None

# order book
def add_order_book(symbol, up_orderbook, down_orderbook):
    session = db_helper_orm.Session()
    try:
        order_book = db_helper_orm.OrderBook(symbol=symbol, up_orderbook=up_orderbook, down_orderbook=down_orderbook)
        session.add(order_book)
        session.commit()
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()
# order flow
def add_order_flow(symbol, side, price, size, trade_time, trade_id):
    session = db_helper_orm.Session()
    try:
        order = db_helper_orm.OrderFlow(symbol=symbol, side=side,
                                                price=price,  size=size, trade_time=trade_time,
                                                trade_id=trade_id)
        session.add(order)
        session.commit()
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()

# order counter
def add_order_counter(symbol, quote_price, trade_price, buy_size, sell_size, buy_counter, sell_counter):
    session = db_helper_orm.Session()
    try:
        order_counter = db_helper_orm.OrderCounter(symbol=symbol, quote_price=quote_price,
                                                trade_price=trade_price,  buy_size=buy_size, sell_size=sell_size,
                                                buy_counter=buy_counter, sell_counter=sell_counter)
        session.add(order_counter)
        session.commit()
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()

# add order flow analyser
def add_order_flow_analyser(symbol, open,close,high,low,buy_counter,
                            sell_counter,buy_size, sell_size,large_buy_counter,
                            large_sell_counter, large_buy_size, large_sell_size,
                            record_time):
    session = db_helper_orm.Session()
    try:
        small_buy = float(buy_size-large_buy_size)
        small_sell = float(sell_size-large_sell_size)
        if small_buy+small_sell == 0:
            return
        if large_buy_size+large_sell_size == 0:
            return
        small_ratio = math.toNearest(small_buy/(small_sell+small_buy), 0.001)
        big_ratio = math.toNearest(float(large_buy_size)/float(large_sell_size+large_buy_size), 0.001)
        order = db_helper_orm.OrderFlowAnalyser(symbol=symbol,
                                                open_price=open,
                                                close_price=close,
                                                high_price=high,
                                                low_price=low,
                                                buy_counter=buy_counter,
                                                sell_counter=sell_counter,
                                                buy_size=buy_size,
                                                sell_size=sell_size,
                                                large_buy_counter=large_buy_counter,
                                                large_sell_counter=large_sell_counter,
                                                large_buy_size=large_buy_size,
                                                large_sell_size=large_sell_size,
                                                small_buy_sell_ratio=small_ratio,
                                                big_buy_sell_ratio=big_ratio,
                                                timestamp=record_time)
        session.add(order)
        session.commit()
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()

#sentiments long short sentiment

#sentiments long short sentiment
def add_coinbase_buy_sell(symbol, buy, sell, timeframe, price):
    session = db_helper_orm.Session()
    added = False
    try:
        buysell = db_helper_orm.CoinfarmBuySellIndicator(symbol=symbol, buy=buy, sell=sell, price=price, timeframe=timeframe)
        session.add(buysell)
        session.commit()
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()

def add_coinbase_trade_counter(symbol, b5, s5, b10, s10, b15, s15, b30, s30, b60, s60, b120, s120):
    session = db_helper_orm.Session()
    added = False
    try:
        counter = db_helper_orm.CoinfarmTradeCounter(symbol=symbol, b5=b5, s5=s5,
                                                     b10=b10, s10=s10, b15=b15, s15=s15,
                                                     b30=b30, s30=s30, b60=b60, s60=s60,
                                                     b120=b120, s120=s120)
        session.add(counter)
        session.commit()
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()


def add_binance_longshort(symbol, timeframe, ratio, long, short, type, time):
    session = db_helper_orm.Session()
    added = False
    try:
        result = session.query(db_helper_orm.BinanceLongshort).filter(
            and_(db_helper_orm.BinanceLongshort.symbol == symbol,
                 db_helper_orm.BinanceLongshort.timeframe == timeframe,
                 db_helper_orm.BinanceLongshort.time == time,
                 db_helper_orm.BinanceLongshort.type == type))
        if result.count() == 0:
            longshort=db_helper_orm.BinanceLongshort(symbol=symbol, timeframe=timeframe, time=time,
                                                     type=type, ratio=ratio, long=long, short=short)
            session.add(longshort)
            session.commit()
            added = True
        else:
            pass
            # print('skip', bucket_time)
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()
    return added

def load_binance_longshort(symbol, timeframe, type, start_time, end_time):
    session = db_helper_orm.Session()
    added = False
    try:
        result = session.query(db_helper_orm.BinanceLongshort).filter(
            and_(db_helper_orm.BinanceLongshort.symbol == symbol,
                 db_helper_orm.BinanceLongshort.timeframe == timeframe,
                 db_helper_orm.BinanceLongshort.time > start_time,
                 db_helper_orm.BinanceLongshort.time <= end_time,
                 db_helper_orm.BinanceLongshort.type == type))
        rows = []
        for r in result:
            row = {
                "symbol": r.symbol,
                "timeframe": r.timeframe,
                "ratio": r.ratio,
                "long": r.long,
                "short": r.short,
                "type": r.type,
                "time": r.time
            }
            rows.append(row)
        return rows
        return result
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()


#history bucket
def add_history_bucket(symbol, interval, bucket_time, open, close, high, low, volume):
    session = db_helper_orm.Session()
    bucket_time = bucket_time.to_pydatetime()
    try:
        result = session.query(db_helper_orm.HistoryBucket).filter(
            and_(db_helper_orm.HistoryBucket.symbol == symbol,
                 db_helper_orm.HistoryBucket.interval == interval,
                 db_helper_orm.HistoryBucket.time == bucket_time))
        if result.count() == 0:
            hisBucket=db_helper_orm.HistoryBucket(symbol=symbol, interval=interval, time=bucket_time, open=open,
                                                 close=close, high=high, low=low, volume=volume)
            session.add(hisBucket)
            session.commit()
        else:
            pass
            # print('skip', bucket_time)
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()

def load_history_bucket(symbol, interval, from_date, to_date):
    session = db_helper_orm.Session()
    try:
        result = session.query(db_helper_orm.HistoryBucket).filter(
            and_(
                db_helper_orm.HistoryBucket.symbol == symbol,
                db_helper_orm.HistoryBucket.interval == interval,
                db_helper_orm.HistoryBucket.time >= from_date,
                db_helper_orm.HistoryBucket.time < to_date
            )
        )
        rows = []
        for r in result:
            row = {
                "symbol": r.symbol,
                "interval": r.interval,
                "date": r.time,
                "open": r.open,
                "close": r.close,
                "high": r.high,
                "low": r.low,
                "volume": r.volume
            }
            rows.append(row)
        return rows
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()
    return None

# ws data
def add_ws_data(symbol, type, value):
    session = db_helper_orm.Session()
    try:
        result = session.query(db_helper_orm.WSData).filter(and_(db_helper_orm.WSData.symbol == symbol, db_helper_orm.WSData.type == type))
        if result.count() == 0:
            wsdata = db_helper_orm.WSData(symbol=symbol, type=type, value=value)
            session.add(wsdata)
            session.commit()
        else:
            wsdata = result.one()
            if wsdata.value != value:
                wsdata.value = value
                wsdata.timestamp = datetime.datetime.now()
                session.commit()
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()


def load_ws_data(symbol, type):
    session = db_helper_orm.Session()
    try:
        result = session.query(db_helper_orm.WSData).filter(
            and_(db_helper_orm.WSData.symbol == symbol, db_helper_orm.WSData.type == type)).first()
        if result is not None:
            try:
                return float(result.value)
            except ValueError:
                return result.value
        return 0
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()

# live table actions
def load_live_orders(status, is_short=True):
    session = db_helper_orm.Session()
    try:
        result = session.query(db_helper_orm.LiveOrders).filter(
            and_(
                db_helper_orm.LiveOrders.status == status,
                db_helper_orm.LiveOrders.is_short == is_short
            )
        )
        grid_list = []
        for r in result:
            grid = Grid(r.symbol, r.desired_open_price, r.desired_close_price,
                        r.desired_quantity, r.status,
                        r.open_id, r.close_id,
                        r.open_quantity, r.close_quantity)
            grid_list.append(grid)
        return grid_list
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()
    return None


def load_overlimit_live_orders(price, is_short=True):
    session = db_helper_orm.Session()
    try:
        result = None
        if is_short:
            result = session.query(db_helper_orm.LiveOrders).filter(
                and_(
                    db_helper_orm.LiveOrders.status == order_active,
                    db_helper_orm.LiveOrders.is_short == is_short,
                    db_helper_orm.LiveOrders.desired_open_price > price
                )
            )
        else:
            result = session.query(db_helper_orm.LiveOrders).filter(
                and_(
                    db_helper_orm.LiveOrders.status == order_active,
                    db_helper_orm.LiveOrders.is_short == is_short,
                    db_helper_orm.LiveOrders.desired_open_price < price
                )
            )
        grid_list = []
        for r in result:
            grid = Grid(r.symbol, r.desired_open_price, r.desired_close_price,
                        r.desired_quantity, r.status,
                        r.open_id, r.close_id,
                        r.open_quantity, r.close_quantity)
            grid_list.append(grid)
        return grid_list
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()
    return None

def check_live_order_already_placed(price, step, interval=1, is_short=True):
    session = db_helper_orm.Session()
    try:
        result = session.query(db_helper_orm.LiveOrders).filter(
            and_(
                db_helper_orm.LiveOrders.is_short == is_short,
                db_helper_orm.LiveOrders.desired_open_price > price - step * interval,
                db_helper_orm.LiveOrders.desired_open_price < price + step * interval
            )
        )
        if result.count() > 0:
            return True
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()
    return False



def add_live_order(desired_open_price, quantity, desired_close_price, open_id, is_short=True):
    session = db_helper_orm.Session()
    try:
        symbol = settings.LONG_SYMBOL
        if is_short:
            symbol = settings.SHORT_SYMBOL
        live_order = db_helper_orm.LiveOrders(
            symbol=symbol,
            desired_open_price=desired_open_price,
            desired_close_price=desired_close_price,
            desired_quantity=quantity,
            open_id=open_id,
            open_time=datetime.datetime.utcnow(),
            open_quantity=quantity,
            status=order_active,
            is_short=is_short
        )
        session.add(live_order)
        session.commit()
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()

def update_live_order(open_id, close_id, open_quantity, is_short=True):
    session = db_helper_orm.Session()
    try:
        result = session.query(db_helper_orm.LiveOrders).filter(
            and_(
                db_helper_orm.LiveOrders.open_id == open_id,
                db_helper_orm.LiveOrders.is_short == is_short
            )
        )
        live_order = result.first()
        if live_order is not None:
            live_order.close_id = close_id
            live_order.status = position_open
            live_order.close_quantity = open_quantity,
            live_order.close_time = datetime.datetime.utcnow()
            session.commit()
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()


def close_live_order(order_id, is_open_id=True, is_short=True):
    session = db_helper_orm.Session()
    try:
        if is_open_id:
            session.query(db_helper_orm.LiveOrders).filter(
                and_(
                    db_helper_orm.LiveOrders.open_id == order_id,
                    db_helper_orm.LiveOrders.is_short == is_short
                )
            ).delete()
        else:
            session.query(db_helper_orm.LiveOrders).filter(
                and_(
                    db_helper_orm.LiveOrders.close_id == order_id,
                    db_helper_orm.LiveOrders.is_short == is_short
                )
            ).delete()
        session.commit()
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()


# history table actions
def open_history_order(quantity, open_id, price, is_short=True):
    session = db_helper_orm.Session()
    try:
        symbol = settings.LONG_SYMBOL
        if is_short:
            symbol = settings.SHORT_SYMBOL
        history_order = db_helper_orm.HistoryOrders(
            symbol=symbol,
            open_id=open_id,
            open_time=datetime.datetime.utcnow(),
            open_price=price,
            open_quantity=quantity,
            is_short=is_short
        )
        session.add(history_order)
        session.commit()
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()


def close_history_order(quantity, open_price, close_price, close_id, open_id, is_short=True):
    session = db_helper_orm.Session()
    profit = (1/open_price - 1/close_price)*(quantity)*close_price
    if is_short:
        profit = (1 / open_price - 1 / close_price) * (-quantity) * close_price
    try:
        result = session.query(db_helper_orm.HistoryOrders).filter(
            and_(
                db_helper_orm.HistoryOrders.open_id == open_id,
                db_helper_orm.HistoryOrders.is_short == is_short
            )
        )
        history_order = result.first()
        if history_order is not None:
            history_order.close_id = close_id
            history_order.close_price = close_price
            history_order.close_quantity = quantity
            history_order.close_time = datetime.datetime.utcnow()
            history_order.profit = profit
            session.commit()
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()


# settings actions
def load_settings(label):
    result = load_settings_object(label)
    if result is None:
        return None
    return result.value

def load_settings_time(label):
    result = load_settings_object(label)
    if result is None:
        return None
    return result.timestamp

def load_settings_object(label):
    session = db_helper_orm.Session()
    try:
        result = session.query(db_helper_orm.Settings).filter(
            and_(
                db_helper_orm.Settings.name == label
            )
        )
        setting = result.first()
        if setting is not None:
            return setting
        else:
            return None
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()



def save_setting(value, label):
    session = db_helper_orm.Session()
    try:
        result = session.query(db_helper_orm.Settings).filter(db_helper_orm.Settings.name == label)
        if result.count() == 0:
            settting = db_helper_orm.Settings(name=label, value=value)
            session.add(settting)
            session.commit()
        else:
            settting = result.first()
            settting.value = value
            settting.timestamp = datetime.datetime.now()
            session.commit()
    except:
        logger.error(sys.exc_info())
    finally:
        session.close()





import pymysql

from datetime import datetime
from datetime import timedelta
from utils import database, math
import sys
from os.path import dirname, join, abspath
sys.path.insert(0, abspath(join(dirname(__file__), '..')))

class OrderCounter():
    def __init__(self, con=None):
        if con is None:
            self.con = pymysql.connect(host='108.61.169.242', user='ziwa', password='Wz062362,.!', db='ziwa_marketmaker', cursorclass=pymysql.cursors.DictCursor)
        else:
            self.con = con

    def process_one_order_counter(self, from_time, to_time, save_db=False, big_order_limit=1000):
        with self.con:
            cur = self.con.cursor()
            cur.execute("SELECT * FROM order_flow where timestamp >= %s and timestamp < %s order by id",
                        (from_time.strftime("%Y-%m-%d %H:%M:%S"), to_time.strftime("%Y-%m-%d %H:%M:%S")))
            rows = cur.fetchall()
            all_buy_count = 0
            all_sell_count = 0
            all_buy_size = 0
            all_sell_size = 0
            large_buy_count = 0
            large_sell_count = 0
            large_buy_size = 0
            large_sell_size = 0
            open_price = float(rows[0]['price'])
            close_price = float(rows[-1]['price'])
            high_price = open_price
            low_price = open_price
            for row in rows:
                if float(row['price']) >= high_price:
                    high_price = float(row['price'])
                if float(row['price']) <= low_price:
                    low_price = float(row['price'])
                if row['side'] == 'Buy':
                    all_buy_size += int(row['size'])
                    all_buy_count += 1
                    if int(row['size']) > big_order_limit:
                        large_buy_size += int(row['size'])
                        large_buy_count += 1
                if row['side'] == 'Sell':
                    all_sell_size += int(row['size'])
                    all_sell_count += 1
                    if int(row['size']) > big_order_limit:
                        large_sell_size += int(row['size'])
                        large_sell_count += 1
            if high_price > 0:
                # if there is data in this time frame
                # print('time', from_time, 'open', open_price, 'close', close_price, 'high', high_price, 'low',
                #       low_price,
                #       'buy count', all_buy_count, 'sell count', all_sell_count, 'buy size', all_buy_size, 'sell size',
                #       all_sell_size,
                #       'large buy count', large_buy_count, 'large sell count', large_sell_count, 'large buy size',
                #       large_buy_size, 'large sell size', large_sell_size)
                if save_db:
                    database.add_order_flow_analyser('XBTUSD', open_price, close_price, high_price, low_price,
                                                     all_buy_count,
                                                     all_sell_count, all_buy_size, all_sell_size, large_buy_count,
                                                     large_sell_count, large_buy_size, large_sell_size, from_time)

    def process_order_counter_group(self, from_time, to_time, delta_second=60, save_db=False, big_order_limit=2000):
        with self.con:
            cur = self.con.cursor()
            query = "SELECT * FROM order_flow where STR_TO_DATE(trade_time, '%Y-%m-%dT%H:%i:%s.%fZ') >= '" \
                    + from_time.strftime("%Y-%m-%d %H:%M:%S") + "' and STR_TO_DATE(trade_time, '%Y-%m-%dT%H:%i:%s.%fZ') < '"\
                    + to_time.strftime("%Y-%m-%d %H:%M:%S") + "' order by id"
            cur.execute(query)
            rows = cur.fetchall()
            index = 0
            current_date = from_time

            while current_date < to_time:
                all_buy_count = 0
                all_sell_count = 0
                all_buy_size = 0
                all_sell_size = 0
                large_buy_count = 0
                large_sell_count = 0
                large_buy_size = 0
                large_sell_size = 0
                open_price = 0
                close_price = 0
                high_price = open_price
                low_price = 9999999
                stop_date = current_date+timedelta(seconds=delta_second)
                for i in range(index, len(rows)):
                    open_price = float(rows[index]['price'])
                    close_price = float(rows[i]['price'])
                    row = rows[i]
                    if datetime.strptime(row['trade_time'], '%Y-%m-%dT%H:%M:%S.%fZ') >= stop_date:
                        index = i
                        break
                    if float(row['price'])>=high_price:
                        high_price = float(row['price'])
                    if float(row['price'])<=low_price:
                        low_price = float(row['price'])
                    if row['side'] == 'Buy':
                        all_buy_size += int(row['size'])
                        all_buy_count += 1
                        if int(row['size']) > big_order_limit:
                            large_buy_size += int(row['size'])
                            large_buy_count += 1
                    if row['side'] == 'Sell':
                        all_sell_size += int(row['size'])
                        all_sell_count += 1
                        if int(row['size']) > big_order_limit:
                            large_sell_size += int(row['size'])
                            large_sell_count += 1
                if high_price > 0:
                    # if there is data in this time frame
                    print('time', current_date, 'open', open_price, 'close', close_price, 'high', high_price, 'low', low_price,
                          'buy count', all_buy_count, 'sell count', all_sell_count, 'buy size', all_buy_size, 'sell size', all_sell_size,
                          'large buy count', large_buy_count, 'large sell count', large_sell_count, 'large buy size', large_buy_size, 'large sell size', large_sell_size)
                    if save_db:
                        database.add_order_flow_analyser('XBTUSD', open_price,close_price,high_price,low_price,all_buy_count,
                                                         all_sell_count,all_buy_size,all_sell_size,large_buy_count,
                                                         large_sell_count,large_buy_size,large_sell_size,current_date)
                current_date += timedelta(seconds=delta_second)

    def process_order_counter_single(self, from_time, to_time, delta_second=60, save_db=False, big_order_limit=2000):
        with self.con:
            cur = self.con.cursor()
            current_date = from_time

            while current_date < to_time:
                session_end = current_date + timedelta(seconds=delta_second)
                cur.execute("SELECT * FROM order_flow where timestamp >= %s and timestamp < %s order by id",
                            (current_date.strftime("%Y-%m-%d %H:%M:%S"), session_end.strftime("%Y-%m-%d %H:%M:%S")))
                rows = cur.fetchall()
                all_buy_count = 0
                all_sell_count = 0
                all_buy_size = 0
                all_sell_size = 0
                large_buy_count = 0
                large_sell_count = 0
                large_buy_size = 0
                large_sell_size = 0
                open_price = float(rows[0]['price'])
                close_price = float(rows[-1]['price'])
                high_price = open_price
                low_price = open_price
                for row in rows:
                    if float(row['price'])>=high_price:
                        high_price = float(row['price'])
                    if float(row['price'])<=low_price:
                        low_price = float(row['price'])
                    if row['side'] == 'Buy':
                        all_buy_size += int(row['size'])
                        all_buy_count += 1
                        if int(row['size']) > big_order_limit:
                            large_buy_size += int(row['size'])
                            large_buy_count += 1
                    if row['side'] == 'Sell':
                        all_sell_size += int(row['size'])
                        all_sell_count += 1
                        if int(row['size']) > big_order_limit:
                            large_sell_size += int(row['size'])
                            large_sell_count += 1
                if len(rows) > 0:
                    print('time', current_date, 'open', open_price, 'close', close_price, 'high', high_price, 'low', low_price,
                          'buy count', all_buy_count, 'sell count', all_sell_count, 'buy size', all_buy_size, 'sell size', all_sell_size,
                          'large buy count', large_buy_count, 'large sell count', large_sell_count, 'large buy size', large_buy_size, 'large sell size', large_sell_size)
                    if save_db:
                        database.add_order_flow_analyser('XBTUSD', open_price,close_price,high_price,low_price,all_buy_count,
                                                         all_sell_count,all_buy_size,all_sell_size,large_buy_count,
                                                         large_sell_count,large_buy_size,large_sell_size,current_date)
                current_date += timedelta(seconds=delta_second)
import os
import time
import settings
from datetime import datetime

def convert_bitmex_date(str):
    return datetime.strptime(str,'%Y-%m-%dT%H:%M:%S.%fZ')

def save_historic_klines_file(df, starttime, endtime, interval):
    fileDir = os.path.dirname(os.path.realpath('__file__'))
    filename = os.path.join(fileDir, 'bitmex_{}_{}_{}-{}.tsv'.format(settings.SYMBOL, interval,
                                                                     starttime.strftime('%Y-%m-%d-%H-%M-%S'),
                                                                     endtime.strftime('%Y-%m-%d-%H-%M-%S')))

    df.to_csv(filename)